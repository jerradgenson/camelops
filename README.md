# Camelops RPN Evaluator


## What it is

Camelops is an RPN evaluator, concatenative mini language, and a darn good
calculator. What does this mean? Let's start at the end — it's a calculator (and
a darn good one).


### A calculator

But it isn't just _any_ calculator. It's a scientific,
programmable, RPN calculator that's designed for extremely efficient number
crunching for those times when you need to crunch numbers "by hand." It comes
with a feature-rich standard library that's extensible by a concatenative mini
language. Which brings us to our second descriptor.


### A concatenative mini language

A mini language is a small, domain-specific language that's very good at a
specific thing — in this case, helping you crunch numbers. "Concatenative" means
that it uses a model of subroutines based on function _composition_ instead of
function _application_, in a similar manner to Forth and Joy, which is a very
efficient model for some problem domains. If it still isn't clear to you what
this means, don't worry. You can gloss over this aspect of Camelops for now and
return to it if/when you need to write an extension in Camelops. You don't need
to understand function composition to _use_ Camelops.


### An RPN evaluator

Finally, Camelops is an RPN evaluator. This is at the heart of what makes Camelops
special. You may already be familiar with RPN — if so, you can skip the rest of
this paragraph. RPN standards for "reverse polish notation," which is a
mathematical notation that, in contrast with algebraic notation, uses postix
operators (where the operator follows its operands) instead of traditional
infix operators (where the operator comes in-between its operands). It is
more efficient both to read and to write expressions in RPN than in standard
algebraic notation.


What's more, the RPN evaluator that Camelops implements is designed for more
than just the Camelops toplevel. In the near future, it will be possible to call
the Camelops RPN evaluator from almost any programming language by way of a
simple, non-linking call mechanism. This feature hasn't been implemented yet,
but it's planned for the near-term.


## Why it is

The long and short of why Camelops exists is because of the dearth of genuinely
_good_ software calculators. The working assumption seems to be that you never
need a "mere" calculator when you have a more general-purpose computer that you
can program for the same jobs a calculator can do and more. This line of
reasoning has merit; in general, when faced with a set of calculations that are
done repeatedly, you should write a program instead of doing it "by hand."


But what about calculations that _aren't_ done repeatedly, or that otherwise
don't lend themselves to a programmatic approach? I find myself reaching for my
trusty HP 35s quite often when I'm sitting in front of a computer. I don't think
I should ever need a dedicated calculator when I'm sitting in front of a
computer.


Of course, I _could_ use the calculator that came bundled with my operating
system, but its functionality is invariably quite limited, and much less
efficient than it could be. I could also use a programming language toplevel,
like ipython. And in fact, this is what I do when I'm without my 35s. But
programming language toplevels, while they can certainly do the job, are not
optimized for it. It's a bit like flying an Apache helicopter to work. Sure,
the Apache is more than up to the task, but it's probably not the ideal approach
for most people, most of the time.

I could also use an existing software RPN calculator, like Emacs Calc, but I
don't feel like I should have to install an ~~operating system~~ text editor
just to get its calculator. Plus, none of the existing ones that I've seen
include a concatenative mini language.


## Goals

Camelops aims to be a total replacement for my HP 35s, but it does _not_ intend
to be some kind of virtual 35s. It's inspired by it, but it's not a clone.
Hopefully it will even be an improvement. Part of this includes implementing a
mini language in which to extend the calculator's built-in library of functions.
The Camelops language is small but Turing complete and well-suited for the task.


A secondary but important goal of Camelops is to provide a set of rich and
powerful tools for evaluating RPN expressions that can be called from almost
any language. This may or may not lead to a fully-fledged GUI or web app at some
point down the road.


## Current State

In its current state, Camelops is working but incomplete. The toplevel is very
basic and not as efficient or user-friendly as it could be. The foreign
language interface hasn't been implemented. The only data type it currently has
is numerics, and support for scientific notation hasn't been added yet. The mini
language is in the early stages — only a few language features have been
implemented, and the grammar isn't stable yet.


That being said, it's completely usable for most basic and some advanced
calculations at this point. I'm using it myself. If Camelops interests you,
there's no reason to wait for a stable release to begin using it. It's easy to
build and run, the features that _are_ implemented work well, and no
currently-working toplevel features should be significantly changed or removed.

See the [Language Reference](https://gitlab.com/jerradgenson/camelops/wikis/Language-Reference)
wiki for a full listing of what features are currently working.


## Setup and Usage

At the moment, pre-compiled binaries aren't available for Camelops. Fortunately,
it is very easy to build, and herein is a correspondingly very easy set of
build instructions. You just run the build script (build.sh) from the project's
root directory, and a camelops executable appears. All intermediary build files
are kept in the build directory so you can easily inspect them or link the RPN
evaluator into your own program. But first, you need to make sure you have the
source code and that a (thankfully) few dependencies are installed.

### Getting Camelops

If you're reading this then you probably already have the Camelops sources, but
in the event that you don't, you can find them at
https://gitlab.com/jerradgenson/camelops. You can clone the project using git
by doing:
```
$ git clone git@gitlab.com:jerradgenson/camelops.git
```
or
```
$ git clone https://gitlab.com/jerradgenson/camelops.git
```

You can also download the source code directly from the aforementioned GitLab
project page.

### Dependencies

* OCaml (>=4.06.0)
* ocamllex (>=4.06.0)
* ocamlyacc (>=4.06.0)
* A reasonably recent Bourne shell
* rlwrap (optional but recommended)

Older versions of these dependencies may (and probably will) work, but haven't
been tested and aren't officially supported. If you're running Linux, the
preferred way to install OCaml is through your system's package manager.
For Ubuntu/Debian, do this:
```
$ sudo apt update
$ sudo apt install ocaml
```

For Fedora, do this:
```
$ sudo dnf install ocaml
```

For other platforms, see the [OCaml website](https://ocaml.org/docs/install.html)
for instructions. Installing OCaml should bring ocamllex and ocamlyacc along
with it.

### Running

As previously mentioned, once you've built Camelops you should have a camelops
executable in your project root. You can run it from anywhere on your system.
If you're on a system that supports rlwrap, you're highly encouraged to call the
camelops executable with it instead of calling it directly, as rlwrap adds
significant improvements to the current toplevel.

For more information about interacting with the Camelops toplevel and working
with the mini language, see the
[Camelops tutorial](https://gitlab.com/jerradgenson/camelops/wikis/tutorial).

### Linking

If you want to link your own program against Camelops, the files you want are
rpn.cmx and rpn.cmi, located in the build directory. See the docstring
for rpn.ml (located in src) for information about using the `Rpn` module.

________________________________________________________________________________


Copyright 2018 Jerrad Michael Genson

This work is licensed under a Creative Commons Attribution-ShareAlike 4.0
International License. To view a copy of this license, visit
https://creativecommons.org/licenses/by-sa/4.0/
