(** Provides a nicer, more friendly interface to the lexer, parser, and semantic
    analyzer. Under most circumstances, this is the interface that should be
    used in lieu of calling any of these modules directly.

    Copyright 2018 Jerrad Michael Genson
    Licensed under the EUPL. See COPYING.ALT for details.
    This file is part of Camelops.
    You may use, redistribute, and/or modify this software under the terms of
    either the GNU Lesser General Public License or the European Union Public
    License. If some or all of the terms of either license is determined to be
    invalid in your jurisdiction, the other license applies instead.

    Camelops is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Camelops is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Camelops.  If not, see <https://www.gnu.org/licenses/>. *)

(** Evaluate an RPN command in the context of the target environment.
    Do not mutate the given environment. Return a copy of the environment
    updated by the result of the RPN evaluation. *)
let eval_environment rpn_command ?environment () =
  let env = match environment with
  | Some e -> e
  | None -> Semantics.make_environment () in
  Parser.eval Lexer.token (Lexing.from_string rpn_command) env

(** Evaluate an RPN command free of context or state. Return the result of the
    RPN evaluation. *)
let eval rpn_command =
  let env = Semantics.make_environment () in
  let result = eval_environment rpn_command ~environment:env () in
  match result with
  | Ok new_env -> Ok new_env.result
  | Error error -> Error error
