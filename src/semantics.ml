(** A collection of functions and data types that define the semantic rules of
    an RPN calculator. Under most circumstances, this module should not be
    called directly by a third-party program; it was designed to be used
    by the parser to link grammar productions with semantics.

    Copyright 2018 Jerrad Michael Genson
    Licensed under the EUPL. See COPYING.ALT for details.
    This file is part of Camelops.
    You may use, redistribute, and/or modify this software under the terms of
    either the GNU Lesser General Public License or the European Union Public
    License. If some or all of the terms of either license is determined to be
    invalid in your jurisdiction, the other license applies instead.

    Camelops is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Camelops is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Camelops.  If not, see <https://www.gnu.org/licenses/>. *)

(* Constants *)
let prompt_indentation = 6
let debug_indentation = prompt_indentation - 1
let debug = false
let max_root_iterations = 10000000
let min_root_precision = 0.000000001
let initial_stack = [0.]


(* Types *)
type ui_action =
  | Exit

(** Type of the stack that holds the results from the user's calculations.
    Included attributes:
    pop_i: Safely pop and return a number of values, i, from the top of the
           stack. If the number of requested values is greater than the size of
           the stack, raise an Evaluation_error (INSUFFICIENT_STACK_SIZE) and
           do not mutate the stack.
    push: Push a value onto the top of the stack.
    clear: Reinitialize the stack with the values defined by `initial_stack`.
    get: Return a list of values currently on the stack.
    index: Return the value at the given stack index, where the bottom value on
           the stack is at index `0`.
    shift: Move the value on the top of the stack to the bottom of the stack. *)
type stack = {
  pop_i : int -> float list;
  push : float -> unit;
  clear : unit -> unit;
  get : unit -> float list;
  set : float list -> unit;
  index : int -> float;
  shift : unit -> unit
}

(** The hash table that holds values predefined or defined by the user along
    with the identifiers they are bound to. Included attributes:
    get_var: Return the value bound to the target variable.
    set_var: Bind the target variable to the given value.
    drop_var: Delete the binding for the target variable.
    copy: Return a value copy of the hash table.
    replace: Replace the entire hash table with a different table.
    *)
type symbol_table = {
  get_var : string -> float;
  set_var : string -> float -> unit;
  drop_var : string -> unit;
  clear_vars : unit -> unit;
  copy : unit -> (string, float) Hashtbl.t;
  replace : (string, float) Hashtbl.t -> unit
}

type environment = {
  result : float option;
  action : ui_action option;
  stack : stack;
  symbol_table: symbol_table
}

type syntax_error = { column_start: int; column_end: int }
type evaluation_error =
  | INVALID_STACK_REFERENCE of int
  | INSUFFICIENT_STACK_SIZE
  | UNBOUND_VARIABLE of string
  | SYNTAX_ERROR of syntax_error
  | DIVISION_BY_ZERO
  | VALUE_ERROR of float * string

type binary_operator =
  | PLUS
  | MINUS
  | MULTIPLY
  | DIVIDE
  | EXPONENT
  | ROOT

type unary_operator =
  | NEGATIVE
  | SIN
  | ASIN
  | SINH
  | COS
  | ACOS
  | COSH
  | TAN
  | ATAN
  | TANH
  | SQ
  | SQRT
  | INV
  | LOG
  | LN
  | ABS
  | SIGN

type semantic_analyzer = {
  statement : ui_action option -> environment;
  expression: float -> environment;
  environment : environment;
  exit : unit -> ui_action option;
  clear : unit -> ui_action option;
  clearall : unit -> ui_action option;
  let_ : float -> string -> ui_action option;
  identifier_drop : string -> ui_action option;
  drop : unit -> ui_action option;
  swap : unit -> ui_action option;
  binary_operation : binary_operator -> ?x:float -> ?y:float -> unit -> float;
  unary_operation : unary_operator -> ?x:float -> unit -> float;
  identifier : string -> float;
  pop : unit -> float;
  index : int -> float;
  shift : unit -> ui_action option
}


(* Exceptions *)
exception Evaluation_error of evaluation_error


(** Make a new stack (of type `stack`) along with its corresponding functions
    and initialize the stack with the values defined by `initial_stack`. *)
let make_stack () =
  let stack = ref initial_stack in
  let push value = stack := (value :: !stack) in
  let clear () = stack := initial_stack in
  let get () = !stack in
  let set new_stack = stack := new_stack in
  let rec index stackref =
    try
      List.nth !stack (List.length !stack - stackref - 1)
    with Invalid_argument _ -> raise (Evaluation_error (
      INVALID_STACK_REFERENCE stackref
      )) in
  let pop i =
    let raw_pop () =
      let head = List.hd !stack in
      let () = stack := List.tl !stack in
      head in
    let rec guarded_pop i =
      if i < 1 then [] else
        try
          let _ = index (i - 1) in
          raw_pop () :: (guarded_pop (i - 1))
        with
        | Evaluation_error _ ->
          raise (Evaluation_error INSUFFICIENT_STACK_SIZE) in
    guarded_pop i in
  let shift () =
    let values = pop 1 in
    let () = assert (List.length values = 1) in
    let head = List.hd values in
    stack := !stack @ [head] in
  { pop_i=pop; push=push; clear=clear; get=get; set=set; index=index; shift=shift }


(** Make a new hash table of type `symbol_table`. *)
let make_symbol_table () =
  let symbol_table = ref (Hashtbl.create 20) in
  let _ = Hashtbl.add !symbol_table "&" 0. in
  let get name =
    try
      Hashtbl.find !symbol_table name
    with Not_found ->
      raise (Evaluation_error (UNBOUND_VARIABLE name)) in
  let set name value = Hashtbl.add !symbol_table name value in
  let clear () = Hashtbl.reset !symbol_table in
  let copy () = Hashtbl.copy !symbol_table in
  let drop_var x = let () = Hashtbl.remove !symbol_table x in () in
  let replace new_table = symbol_table := new_table in {
    get_var=get;
    set_var=set;
    drop_var=drop_var;
    clear_vars=clear;
    copy=copy;
    replace=replace
  }

(** Find the nth root of a. *)
let nth_root a n =
   let n1 = n -. 1.0 in
   let rec iter x =
      let x' = (n1 *. x +. a /. (x ** n1)) /. n in
      if min_root_precision > abs_float (x -. x') then x' else iter x' in
   iter 1.0

(** A safe version of `nth_root`. Raise an `Evaluation_error (VALUE_ERROR)`
    if `a < 0` or `n < 1`. *)
let safe_root a n =
  if a >= 0. then
    if floor n = n && n >= 1. then
      nth_root a n
    else
      raise (Evaluation_error (VALUE_ERROR (n, "root")))
  else
    raise (Evaluation_error (VALUE_ERROR (a, "root")))

(** A safe version of `( / )`. Raise an `Evaluation_error (DIVISION_BY_ZERO)`
    if `y = 0`. *)
let safe_divide x y =
  if y = 0. then
    raise (Evaluation_error (DIVISION_BY_ZERO))
  else x /. y

(** Pop one value off the top of the stack and return it. *)
let pop_one stack =
  let values = stack.pop_i 1 in
  let () = assert (List.length values = 1) in
  List.hd values

(** Pop two values off the top of the stack and return them as a list. *)
let pop_two stack =
  let values = stack.pop_i 2 in
  let () = assert (List.length values = 2) in
  (List.hd values, List.nth values 1)

(** Match a binary operator with the semantic action (function) that it's
    associated with. *)
let match_binary_operator binary_operator = match binary_operator with
  | PLUS -> ( +. )
  | MINUS -> ( -. )
  | MULTIPLY -> ( *. )
  | DIVIDE -> safe_divide
  | EXPONENT -> ( ** )
  | ROOT -> safe_root

(** Apply the given binary operator function to the optional values `x` and `y`.
    If `y` is not given, pop a value off the stack and use it for `y`.
    If neither `x` nor `y` are given, pop two values off the stack and use them
    in place of `x` and `y`. If an argument is given for `y`, an argument must
    be given for `x` also. *)
let apply_binary_operator stack f ?x ?y () =
  let () = if y != None then assert (x != None) else () in
  match x with
  | Some value_x -> begin match y with
    | Some value_y -> f value_x value_y
    | None -> f (pop_one stack) value_x
    end
  | None -> let (x, y) = pop_two stack in f x y

(** Safely divide 1 by x. If `x = 0`, raise an `Evaluation_error
    (DIVISION_BY_ZERO)`. *)
let safe_invert x =
  if x = 0. then
    raise (Evaluation_error DIVISION_BY_ZERO)
    else 1. /. x

(** Match a unary operator with the semantic action (function) that it's
    associated with. *)
let match_unary_operator unary_operator = match unary_operator with
  | NEGATIVE -> fun x -> -1. *. x
  | SIN -> sin
  | ASIN -> asin
  | SINH -> sinh
  | COS -> cos
  | ACOS -> acos
  | COSH -> cosh
  | TAN -> tan
  | ATAN -> atan
  | TANH -> tanh
  | SQ -> fun x -> x ** 2.
  | SQRT -> sqrt
  | INV -> safe_invert
  | LOG -> log10
  | LN -> log
  | ABS -> abs_float
  | SIGN -> fun x -> if x >= 0. then 1. else -1.

(** Apply the given unary operator function to the optional value `x`.
    If `x` is not given, pop a value off the stack and use it for `x`. *)
let apply_unary_operator stack f ?x () =
  match x with
  | Some value_x -> f value_x
  | None -> f (pop_one stack)


(* Public definitions *)
(* Functions that bind directly to parser productions. *)
let list_of_stack stack = stack.get ()
let make_environment () =
  {
    result=None;
    action=None;
    stack=make_stack ();
    symbol_table=make_symbol_table ()
  }

let make_analyzer environment =
  let pop () = pop_one environment.stack in
  let index = environment.stack.index in
  let drop () = let _ = pop () in None in
  let shift () = let () = environment.stack.shift () in None in
  let swap () =
    let (x, y) = pop_two environment.stack in
    let () = environment.stack.push y in
    let () = environment.stack.push x in
    None in
  let clear ()= let () = environment.stack.clear () in None in
  let let_ value id = let () = environment.symbol_table.set_var id value in None in
  let identifier = environment.symbol_table.get_var in
  let identifier_drop id = let () = environment.symbol_table.drop_var id in None in
  let clearall () =
    let () = environment.stack.clear () in
    let () = environment.symbol_table.clear_vars () in
    None in
  let exit () = Some Exit in
  let unary_operation unary_operator =
    apply_unary_operator environment.stack (match_unary_operator unary_operator) in
  let binary_operation binary_operator =
    apply_binary_operator environment.stack (match_binary_operator binary_operator) in
  let statement action = {
    result=None;
    action=action;
    stack=environment.stack;
    symbol_table=environment.symbol_table
  } in
  let expression value =
    let () = environment.stack.push value in {
      result=Some value;
      action=None;
      stack=environment.stack;
      symbol_table=environment.symbol_table
    } in
    {
      statement=statement;
      expression=expression;
      environment=environment;
      exit=exit;
      clear=clear;
      clearall=clearall;
      let_=let_;
      identifier_drop=identifier_drop;
      drop=drop;
      swap=swap;
      binary_operation=binary_operation;
      unary_operation=unary_operation;
      identifier=identifier;
      pop=pop;
      index=index;
      shift=shift
    }

let copy_environment environment =
  let stack_copy = make_stack () in
  let () = stack_copy.set (environment.stack.get ()) in
  let symbol_table_copy = make_symbol_table () in
  let () = symbol_table_copy.replace (environment.symbol_table.copy ()) in
{
  result=environment.result;
  action=environment.action;
  stack=stack_copy;
  symbol_table=symbol_table_copy
}
