(** Public interface to the Camelops parser. The functions and types defined here
    should not normally be used directly; the functions in the Rpn module should
    be called instead.

    Copyright 2018 Jerrad Michael Genson
    Licensed under the EUPL. See COPYING.ALT for details.
    This file is part of Camelops.
    You may use, redistribute, and/or modify this software under the terms of
    either the GNU Lesser General Public License or the European Union Public
    License. If some or all of the terms of either license is determined to be
    invalid in your jurisdiction, the other license applies instead.

    Camelops is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Camelops is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Camelops.  If not, see <https://www.gnu.org/licenses/>. *)

(** Lexer tokens recognized by the parser. *)
type token =
  | NUMBER of (float)
  | STACK_INDEX of (int)
  | IDENTIFIER of (string)
  | BINARY_OPERATOR of (Semantics.binary_operator)
  | UNARY_OPERATOR of (Semantics.unary_operator)
  | EOF
  | EXIT
  | LET
  | POP
  | CLEAR
  | CLEARALL
  | DROP
  | SWAP
  | SHIFT

(** Parse and evaluate an RPN command encapsulated by a lexbuf with the given
    lexer and environment. This version of eval raises exceptions. *)
val eval_exn : (Lexing.lexbuf -> token) -> Lexing.lexbuf ->
Semantics.environment -> Semantics.environment

(** Parse and evaluate an RPN command encapsulated by a lexbuf with the given
    lexer and environment. This version of eval does not raise exceptions;
    instead, it returns the result type described below. *)
val eval : (Lexing.lexbuf -> token) -> Lexing.lexbuf ->
Semantics.environment ->
(Semantics.environment, Semantics.evaluation_error) result
