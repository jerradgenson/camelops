(** A collection of functions and data types that define the semantic rules of
    the Camelops language. Under most circumstances, this module should not be
    called directly by a third-party program; it was designed to be used
    by the parser to link grammar productions with semantics.

    Copyright 2018 Jerrad Michael Genson
    Licensed under the EUPL. See COPYING.ALT for details.
    This file is part of Camelops.
    You may use, redistribute, and/or modify this software under the terms of
    either the GNU Lesser General Public License or the European Union Public
    License. If some or all of the terms of either license is determined to be
    invalid in your jurisdiction, the other license applies instead.

    Camelops is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Camelops is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Camelops.  If not, see <https://www.gnu.org/licenses/>. *)

(** Amount of whitespace to precede the CLI UI prompt. *)
val prompt_indentation : int

(** Amount of whitespace to precede debug and error messages from the CLI. *)
val debug_indentation : int

(** Set to true to print debug messages; false to surpress them. *)
val debug : bool

(** User input that needs to be handled by the UI. *)
type ui_action =
  | Exit

(** Type of the stack that holds the results from the user's calculations. *)
type stack

(** The hash table that holds values predefined or defined by the user along
    with the identifiers they are bound to. *)
type symbol_table

(** A record of data structures that represents a complete calculator
    environment. Record values:
    result: The numeric result of the previous calculation, if any.
    action: See the definition of `ui_action` above.
    stack: See the definition of `stack` above.
    symbol_table: See the definition of `symbol_table` above.
    *)
type environment = private {
  result : float option;
  action : ui_action option;
  stack : stack;
  symbol_table : symbol_table
}

(** Raised along with syntax errors. Contains a record with the start and end
    index of the error in the input string. *)
type syntax_error = { column_start: int; column_end: int }

(** The type of Evaluation_errors that can be raised. *)
type evaluation_error =
  | INVALID_STACK_REFERENCE of int
  | INSUFFICIENT_STACK_SIZE
  | UNBOUND_VARIABLE of string
  | SYNTAX_ERROR of syntax_error
  | DIVISION_BY_ZERO
  | VALUE_ERROR of float * string

(** The token type of binary operators. *)
type binary_operator =
  | PLUS
  | MINUS
  | MULTIPLY
  | DIVIDE
  | EXPONENT
  | ROOT

(** The token type of unary operators. *)
type unary_operator =
  | NEGATIVE
  | SIN
  | ASIN
  | SINH
  | COS
  | ACOS
  | COSH
  | TAN
  | ATAN
  | TANH
  | SQ
  | SQRT
  | INV
  | LOG
  | LN
  | ABS
  | SIGN

(** A stateful semantic analyzer designed to operate within a particular
    environment. Has the following attributes:
    statement: Semantic rule for the `statement` production.
    expression: Semantic rule for the `expression` production.
    environment: The environment the semantic analyzer operates within.
    exit: Semantic rule for the `exit` production.
    clear: Semantic rule for the `clear` production.
    clearall: Semantic rule for the `clearall` production.
    let_: Semantic rule for the `let` production.
    identifier_drop: Semantic rule for the `drop` production when preceded by
                     an identifier.
    drop: Semantic rule for the `drop` production when not preceded by an
          identifier.
    swap: Semantic rule for the `swap` production.
    binary_operation: Semantic rule for all binary operations defined by the
                      `binary_operator` type.
    unary_operation: Semantic rule for all unary operations defined by the
                     `unary_operator` type.
    identifier: Semantic rule for the `identifier` production.
    pop: Semantic rule for the `pop` production.
    index: Semantic rule for the `index` production.
    shift: Semantic rule for the `shift` production. *)
type semantic_analyzer = private {
  statement : ui_action option -> environment;
  expression: float -> environment;
  environment : environment;
  exit : unit -> ui_action option;
  clear : unit -> ui_action option;
  clearall : unit -> ui_action option;
  let_ : float -> string -> ui_action option;
  identifier_drop : string -> ui_action option;
  drop : unit -> ui_action option;
  swap : unit -> ui_action option;
  binary_operation : binary_operator -> ?x:float -> ?y:float -> unit -> float;
  unary_operation : unary_operator -> ?x:float -> unit -> float;
  identifier : string -> float;
  pop : unit -> float;
  index : int -> float;
  shift : unit -> ui_action option;
}


(** Raised when an error is encountered in the lexical analysis, parsing, or
    semantic analysis of user input. *)
exception Evaluation_error of evaluation_error

(** Take stack (as defined above) and return its values in a list. *)
val list_of_stack : stack -> float list

(** Make a new environment initialized with empty or None types. *)
val make_environment : unit -> environment

(** Create a new semantic analyzer with an environment initialized to None types
    and empty collections. *)
val make_analyzer : environment -> semantic_analyzer

(** Create a value copy of the given environment. *)
val copy_environment : environment -> environment
