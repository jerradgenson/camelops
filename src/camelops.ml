(** The toplevel to the Camelops RPN Evaluator. Provides a feature-rich RPN
    (reverse polish notation) calculator on the command line, complete with an
    arbitrary-size stack, variables, compound expressions, and numerous commands
    for manipulating the stack and user interface.

    Copyright 2018 Jerrad Michael Genson
    Licensed under the EUPL. See COPYING.ALT for details.
    This file is part of Camelops.
    You may use, redistribute, and/or modify this software under the terms of
    either the GNU Lesser General Public License or the European Union Public
    License. If some or all of the terms of either license is determined to be
    invalid in your jurisdiction, the other license applies instead.

    Camelops is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Camelops is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Camelops.  If not, see <https://www.gnu.org/licenses/>. *)

open Semantics

(** Licensing info that prints when the Camelops toplevel starts. *)
let license_info = "
               Camelops version 0.5.1
        Copyright 2018 Jerrad Michael Genson
    This program comes with ABSOLUTELY NO WARRANTY.
    This is free software, and you are welcome to
    redistribute it under the terms of either the
    LGPL-3.0-or-later or the EUPL-1.2-or-later. By
    using this program, you agree to these terms.
    If some or all of the terms of either license
    is determined to be invalid in your jurisdiction,
    the other license applies instead.


"

(** The number of newlines to print when clearing the screen. *)
let clear_lines = 100

(** Leading whitespace to print before the command line prompt. *)
let leading_whitespace = String.make (prompt_indentation - 1) ' '

(** Print the command line prompt. *)
let print_prompt () =
  let _ = print_string (String.make prompt_indentation ' ' ^ "# ") in
  flush stdout

(** Pretty-print an RPN environment stack. *)
let rec print_stack stack =
  match stack with
    | hd :: tl ->
      let () = print_stack tl in
      let stackref = string_of_int (List.length stack - 1) in
      print_endline (leading_whitespace ^ "s" ^ stackref ^ ": " ^ (string_of_float hd))
    | [] -> ()

(** Attempt to clear all text from the terminal screen. *)
let clear_screen () =
  let rec clear_screen lines =
    if lines > 0 then
      let () = print_newline () in
      clear_screen (lines - 1)
    else () in
  clear_screen clear_lines

(** Handle an error by printing the appropriate message to the screen and then
    calling main with the target environment to recover and continue. *)
let _handle_error main environment error =
  let error_msg = match error with
  | SYNTAX_ERROR syntax_error ->
                            "Error: Invalid syntax from character "
                            ^ (string_of_int syntax_error.column_start)
                            ^ " through character "
                            ^ (string_of_int syntax_error.column_end)
  | INVALID_STACK_REFERENCE stackref ->
                                  "Error: Stack reference 's"
                                  ^ (string_of_int stackref)
                                  ^ "' does not exist"
  | INSUFFICIENT_STACK_SIZE ->
                      "Error: Too few values on stack to perform operation"
  | UNBOUND_VARIABLE id -> "Error: Variable '"
                           ^ id
                           ^ "' does not exist"
  | DIVISION_BY_ZERO -> "Error: Division by zero"
  | VALUE_ERROR (value, name) ->
                            "Error: The value '"
                            ^ (string_of_float value)
                            ^ "' is not within the domain of the function '"
                            ^ name
                            ^ "'" in
  let () = print_endline (leading_whitespace ^ error_msg) in
  let () = print_newline () in
  let () = flush stdout in
  main environment

(** Process the result returned by a call to `Rpn.eval_environment`.

    Args
      result: A result returned by `Rpn.eval_environment`.
      ok_callback: A function to call after processing an ok result.
      error_callback: A function to call after processing an error result.

    Returns
      Whatever value `ok_callback` and `error_callback` return. *)
let _process_result result ok_callback error_callback = match result with
  | Ok new_environment -> begin match new_environment.action with
    | Some Exit -> ()
    | None ->
        let () = if debug then () else clear_screen () in
        ok_callback new_environment
    end
  | Error error -> error_callback error

(** An event loop that precedes from infinite recursion to get an input string
    from the user, evaluate it as an RPN command, and process the result. *)
let rec _main environment =
  let () = print_stack (list_of_stack environment.stack) in
  let () = print_prompt () in
  try
    let user_input = input_line stdin in
    let result = Rpn.eval_environment user_input ~environment:environment () in
    _process_result result _main (_handle_error _main environment)
  with End_of_file -> print_newline ()

(** The main point of entry for the program. A mere convenience wrapper around
    `_main` that provides a main function which can be called with no args. *)
let main () =
  let () = print_string license_info in _main (make_environment ())

let () = main ()
