{
(** Defines the lexical specification of a valid Camelops command using lexer
    tokens provided by "parser.mli". This file is not an OCaml program itself —
    it is input to ocamllex, the OCaml lexer generator. The output of ocamllex
    is an .ml file that can be compiled and linked with another OCaml program.
    The output of ocamllex should be named "lexer.ml" so it can be found by
    the other modules in Camelops.

    Copyright 2018 Jerrad Michael Genson
    Licensed under the EUPL. See COPYING.ALT for details.
    This file is part of Camelops.
    You may use, redistribute, and/or modify this software under the terms of
    either the GNU Lesser General Public License or the European Union Public
    License. If some or all of the terms of either license is determined to be
    invalid in your jurisdiction, the other license applies instead.

    Camelops is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Camelops is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Camelops.  If not, see <https://www.gnu.org/licenses/>. *)

open Parser
open Semantics

module Token_map = Map.Make(String)

(** Create a map from a list of (string_pattern, token_type) pairs. *)
let rec make_token_map map items =
  match items with
    | (key, value) :: tail ->
      make_token_map (Token_map.add key value map) tail
    | [] -> map

(** A list of (string_pattern, token_type) pairs defining keywords. A keyword in
    the Camelops language is a reserved word that may specify any action the
    Camelops RPN evaluator is capable of taking. *)
let keywords = make_token_map Token_map.empty
  [
    ("exit", EXIT);
    ("let", LET);
    ("pop", POP);
    ("clear", CLEAR);
    ("clearall", CLEARALL);
    ("drop", DROP);
    ("swap", SWAP);
    ("shift", SHIFT)
  ]

(** A list of (string_pattern, token_type) pairs defining binary operators. A
    binary operator in the Camelops language is a reserved word and a function
    that takes two numbers and returns one number without causing any
    side-effects. Note: this is not an exhaustive list of Camelops binary
    operators. These are only the binary operators that are composed of letters,
    i.e. binary operators like '+', '-', '*', etc. are not included. *)
let binary_operators = make_token_map Token_map.empty
  [
    ("root", BINARY_OPERATOR ROOT)
  ]

(** A list of (string_pattern, token_type) pairs defining unary operators. A
    unary operator in the Calemops language is a reserved word and a function
    that takes one number and returns one number without causing any
    side-effects. Note: this is not an exhaustive list of Camelops unary
    operators. These are only the unary operators that are composed of letters,
    i.e. unary operators like '~' and '^' are not included. *)
let unary_operators = make_token_map Token_map.empty
  [
    ("sin", UNARY_OPERATOR SIN);
    ("asin", UNARY_OPERATOR ASIN);
    ("sinh", UNARY_OPERATOR SINH);
    ("cos", UNARY_OPERATOR COS);
    ("acos", UNARY_OPERATOR ACOS);
    ("cosh", UNARY_OPERATOR COSH);
    ("tan", UNARY_OPERATOR TAN);
    ("atan", UNARY_OPERATOR ATAN);
    ("tanh", UNARY_OPERATOR TANH);
    ("sq", UNARY_OPERATOR SQ);
    ("sqrt", UNARY_OPERATOR SQRT);
    ("inv", UNARY_OPERATOR INV);
    ("log", UNARY_OPERATOR LOG);
    ("ln", UNARY_OPERATOR LN);
    ("abs", UNARY_OPERATOR ABS);
    ("sign", UNARY_OPERATOR SIGN)
  ]

(** Take a token as an argument and return its string representation. *)
let string_of_token token =
  match token with
    NUMBER num -> "number = '" ^ (string_of_float num) ^ "'"
  | IDENTIFIER id -> "identifier = '" ^ id ^ "'"
  | BINARY_OPERATOR _ -> "binary_operator"
  | UNARY_OPERATOR _ -> "unary_operator"
  | EXIT -> "keyword = 'exit'"
  | LET -> "keyword = 'let'"
  | POP -> "keyword = 'pop'"
  | CLEAR -> "keyword = 'clear'"
  | CLEARALL -> "keyword = 'clearall'"
  | DROP -> "keyword = 'drop'"
  | SWAP -> "keyword = 'swap'"
  | SHIFT -> "keyword = 'shift'"
  | STACK_INDEX index -> "stack_index = 's" ^ (string_of_int index) ^ "'"
  | EOF -> "eof"

(** Print a token to standard output. *)
let print_token token =
  if debug then
    let leading_ws = String.make debug_indentation ' ' in
    let _ = print_endline (leading_ws ^ "TERMINAL: " ^ (string_of_token token)) in
    flush stdout
  else ()

}

(** Regular expression for upper and lowercase letters. *)
let letter = ['a'-'z'] | ['A'-'Z']

(** Regular rexpression for indivudal numeric digits. *)
let digit = ['0'-'9']

(** Regular expression for a real number in decimal notation. *)
let real_number = '-'?digit*"."digit+ | digit+"."?

(* Start of the actual lexer specification. *)
rule token = parse
  (* Matches tabs and spaces, which are ignored. *)
  | [' ' '\t'] { token lexbuf }

  (* Matches stack references. *)
  | 's'digit+ as stack_index_string {
    let stack_index =
      int_of_string
        (String.sub stack_index_string 1
          (String.length stack_index_string - 1)) in
    let stack_index_token = STACK_INDEX stack_index in
    print_token stack_index_token;
    stack_index_token
    }

  (* Matches real numbers in decimal notation. *)
  | real_number as num {
    let number_token = NUMBER (float_of_string num) in
    print_token number_token;
    number_token
  }

  (* Matches the `PLUS` binary operator. *)
  | '+' {
    let binop_token = BINARY_OPERATOR PLUS in
    print_token binop_token;
    binop_token
  }

  (* Matches the `MINUS` binary operator. *)
  | '-' {
    let binop_token = BINARY_OPERATOR MINUS in
    print_token binop_token;
    binop_token
  }

  (* Matches the `MULTIPLY` binary operator. *)
  | '*' {
    let binop_token = BINARY_OPERATOR MULTIPLY in
    print_token binop_token;
    binop_token
  }

  (* Matches the `DIVIDE` binary operator. *)
  | '/' {
    let binop_token = BINARY_OPERATOR DIVIDE in
    print_token binop_token;
    binop_token
  }

  (* Matches the `EXPONENT` binary operator. *)
  | '^' | '*''*' {
    let binop_token = BINARY_OPERATOR EXPONENT in
    print_token binop_token;
    binop_token
  }

  (* Matches the `NEGATIVE` unary operator. *)
  | '~' {
    let unop_token = UNARY_OPERATOR NEGATIVE in
    print_token unop_token;
    unop_token
  }

  (* Matches identifiers and reserved words. *)
  | letter+(letter | digit | "_")* as id {
    (* Try to match the lexeme with a keyword. *)
    try
      let keyword_token = Token_map.find id keywords in
      print_token keyword_token;
      keyword_token

    with Not_found ->
      (* Try to match the lexeme with a unary operator. *)
      try
        let unop_token = Token_map.find id unary_operators in
        print_token unop_token;
        unop_token

      with Not_found ->
        (* Try to match the lexeme with a binary operator. *)
        try
          let binop_token = Token_map.find id binary_operators in
          print_token binop_token;
          binop_token

        with Not_found ->
          (* Lexeme is not a reserved word, so treat it as an identifier. *)
          let identifier_token = IDENTIFIER id in
          print_token identifier_token;
          identifier_token
  }
  (* Matches `EOF` patterns. *)
  | eof | '\n' { print_token EOF; EOF }

  (* Matches any other expression, which is a syntax error. *)
  | _ {
    raise (Evaluation_error (SYNTAX_ERROR {
        column_start=Lexing.lexeme_start lexbuf;
        column_end=Lexing.lexeme_end lexbuf
      }))
  }
