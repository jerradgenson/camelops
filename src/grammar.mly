%{
(** Defines the grammar of a valid Camelops command and links parser productions
    to semantic actions. This file is not an OCaml program itself — it is input
    to ocamlyacc, the OCaml LALR parser generator. The output of ocamlyacc is an
    .ml file that can be compiled and linked with another OCaml program. The
    output of ocamlyacc should be named "parser.ml" so it can be used with
    the "parser.mli" interface.

    Copyright 2018 Jerrad Michael Genson
    Licensed under the EUPL. See COPYING.ALT for details.
    This file is part of Camelops.
    You may use, redistribute, and/or modify this software under the terms of
    either the GNU Lesser General Public License or the European Union Public
    License. If some or all of the terms of either license is determined to be
    invalid in your jurisdiction, the other license applies instead.

    Camelops is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Camelops is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Camelops.  If not, see <https://www.gnu.org/licenses/>. *)

open Semantics

(** Print symbols (nonterminals) registered by the parser to standard output
    when `Semantics.debug = true`. *)
let print_symbol symbol =
  if debug then
    let leading_ws = String.make debug_indentation ' ' in
    let _ = print_endline (leading_ws ^ "NONTERMINAL: " ^ symbol) in
    flush stdout
  else ()

(** A module-level variable used to associate grammar productions with their
    corresponding semantic actions and evaluation environment. Together with
    the parser (described below) and lexer, this forms a complete evaluator. *)
let semantic_analyzer = ref (make_analyzer (make_environment ()))


%}

/* These tokens define terminal symbols in the Camelops grammar. Each of them
   has one or more corresponding productions, defined by the grammar below. See
   the documentation for the productions for details of what the individual
   tokens mean. */
%token <float> NUMBER
%token <int> STACK_INDEX
%token <string> IDENTIFIER
%token <Semantics.binary_operator> BINARY_OPERATOR
%token <Semantics.unary_operator> UNARY_OPERATOR
%token EOF
%token EXIT LET POP CLEAR CLEARALL DROP SWAP SHIFT

%start command
%type <Semantics.environment> command
%%

/* Delineates the start symbol, command, which describes a single, complete,
   and valid Camelops program. */
command:

  /* End of file with no expressions or statements. */
  | EOF { !semantic_analyzer.statement None }

  /* A statement followed by an end of file character. Note that Camelops does
     not have a notion of compound statements, so this is limited to a single
     statement. */
  | statement EOF { print_symbol "command"; !semantic_analyzer.statement $1 }

  /* An expression followed by an end of file character. In the case of
     expressions, Camelops *does* have a notion of compound expressions, via
     the recursive definition of that symbol, so multiple expressions in some
     cases are valid. */
  | expression EOF { print_symbol "command"; !semantic_analyzer.expression $1 };

  /* Handles an error resulting from invalid syntax. */
  | error EOF {
    raise (Evaluation_error (SYNTAX_ERROR {
        column_start=Parsing.symbol_start ();
        column_end=Parsing.symbol_end ()
      }))
    };

/* A "statement" in the Camelops grammar is a component of a command that does
   not evaluate to anything, and therefore does not push a number onto the stack
   or return a result. */
statement:

  /* EXIT production - terminate the program. */
  | EXIT { print_symbol "statement"; !semantic_analyzer.exit () }

  /* CLEAR production - drop all values on the stack and reinitialize it. */
  | CLEAR { print_symbol "statement"; !semantic_analyzer.clear () }

  /* CLEARALL production - clear both the stack and the symbol table. */
  | CLEARALL { print_symbol "statement"; !semantic_analyzer.clearall () }
  | expression IDENTIFIER LET {
    print_symbol "statement";
    !semantic_analyzer.let_ $1 $2
  }

  /* IDENTIFIER DROP production - remove an id from the symbol table. */
  | IDENTIFIER DROP
    { print_symbol "statement"; !semantic_analyzer.identifier_drop $1 }

  /* DROP production - pop the value off the top of the stack and discard it. */
  | DROP { print_symbol "statement"; !semantic_analyzer.drop () }

  /* SWAP production - swap the top two values on the stack. */
  | SWAP { print_symbol "statement"; !semantic_analyzer.swap () }

  /* SHIFT production - move the value on top of the stack to the bottom of
     the stack.*/
  | SHIFT { print_symbol "statement"; !semantic_analyzer.shift () }

/* An "expression" in the Camelops grammar is a component of a command that does
   evaluate to something and therefore does push a number onto the stack and
   return a value. Unlike statements, expressions have a recursive form known as
   a "compound expression" which is defined in terms of one or more other
   expressions, ad infinitum. */
expression:

  /* BINARY_OPERATOR production form 1 - apply the action defined by
     `BINARY_OPERATOR` to the value of the two preceeding expressions. The
     expression value immediately preceeding `BINARY_OPERATOR` is the "x" value,
     and the other expression value is the "y" value. */
  | expression expression BINARY_OPERATOR {
    print_symbol "expression";
    !semantic_analyzer.binary_operation $3 ~x:$1 ~y:$2 ()
  }

  /* BINARY_OPERATOR production form 2 - apply the action defined by
     `BINARY_OPERATOR` to the value of the preceeding expression (the "x" value)
     and the value at the top of the stack (the "y" value). */
  | expression BINARY_OPERATOR {
    print_symbol "expression";
    !semantic_analyzer.binary_operation $2 ~x:$1 ()
  }

  /* BINARY_OPERATOR production form 3 - apply the action defined by
     `BINARY_OPERATOR` to the value at the top of the stack (the "x" value) and
     the value immediately below it (the "y" value). */
  | BINARY_OPERATOR {
    print_symbol "expression";
    !semantic_analyzer.binary_operation $1 ()
  }

  /* UNARY_OPERATOR production form 1 - apply the action defined by
     `UNARY_OPERATOR` to the value of the preceeding expression. */
  | expression UNARY_OPERATOR {
    print_symbol "expression";
    !semantic_analyzer.unary_operation $2 ~x:$1 ()
  }

  /* UNARY_OPERATOR production form 2 - apply the action befined by
     `UNARY_OPERATOR` to the value at the top of the stack. */
  | UNARY_OPERATOR {
    print_symbol "expression";
    !semantic_analyzer.unary_operation $1 ()
  }

  /* NUMBER production - simply evaluates to the value of `NUMBER`. */
  | NUMBER { print_symbol "expression"; $1 }


  /* IDENTIFIER production - evaluates to the value bound to `IDENTIFIER` in the
                symbol table, if such a binding exists. */
  | IDENTIFIER { print_symbol "expression"; !semantic_analyzer.identifier $1 }

  /* POP production - pop the topmost value from the stack and return it. */
  | POP { print_symbol "expression"; !semantic_analyzer.pop () }

  /* STACK_INDEX production - evaluates to the value located at
     `STACK_INDEX`, if any. `STACK_INDEX` is an integer indicating the
     position of values on the stack starting with 0, which indicates the value
     at the bottom of the stack, to {stack_size - 1}, indicating the value at
     the top of the stack. */
  | STACK_INDEX {
    print_symbol "expression";
    !semantic_analyzer.index $1
  }

%%

let eval_exn lexer lexbuf environment =
  let new_environment = copy_environment environment in
  (* Modify module-level variable to point to a new semantic analyzer. This is
     how the parser knows what semantic analyzer to use. *)
  let () = semantic_analyzer := make_analyzer new_environment in
  command lexer lexbuf

let eval lexer lexbuf environment =
  try
    Ok (eval_exn lexer lexbuf environment)
  with Evaluation_error error ->
    Error error
