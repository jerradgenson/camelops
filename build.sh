# Build script for the Camelops RPN evaluator.
#
# Copyright 2018 Jerrad Michael Genson
# Licensed under the EUPL. See COPYING.ALT for details.
# This file is part of Camelops.
# You may use, redistribute, and/or modify this software under the terms of
# either the GNU Lesser General Public License or the European Union Public License.
# If some or all of the terms of either license is determined to be
# invalid in your jurisdiction, the other license applies instead.
#
# Camelops is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Camelops is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with Camelops.  If not, see <https://www.gnu.org/licenses/>.

rm camelops
rm -fr build
cp -r src build
cd build
ocamlc -c semantics.mli
ocamlopt -c  semantics.ml
ocamlyacc grammar.mly
mv grammar.ml parser.ml
ocamlc -c parser.mli
ocamllex -o lexer.ml lexerspec.mll
ocamlopt -c lexer.ml
ocamlopt -c parser.ml
ocamlopt -c rpn.ml
ocamlopt -c camelops.ml
ocamlopt -o camelops semantics.cmx lexer.cmx parser.cmx rpn.cmx camelops.cmx
rm camelops.ml grammar.mli lexerspec.mll parser.mli rpn.ml semantics.ml grammar.mly semantics.mli
cd ..
mv build/camelops .
